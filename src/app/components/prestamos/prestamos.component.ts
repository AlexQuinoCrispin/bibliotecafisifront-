import { Component, OnInit } from '@angular/core';
import { LibrosService } from '../../services/libros.service';
import { Router } from '@angular/router';
import { Prestamo } from '../../interfaces/prestamo.interface';
import { PrestamoService } from '../../services/prestamo.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css']
})
export class PrestamosComponent implements OnInit {
  public autentificado;  
  public tipoUsuario;
  libros: any[] = [];  
  itemData: any[] = []; 
  libroBoton;
  prestamPost : Prestamo;
  prestamo : Prestamo = {
    usuarioId: '',
    itemId: '',
    fechaInicio: '',
    fechafin: '',
    estado: '',
    adminId: '',
  }

  constructor(private librosService: LibrosService,
    private prestamoService : PrestamoService,
    private autentificacion: AuthenticationService,
    private router: Router) {
      this.librosService.getLibros()
      .subscribe( data => {
      this.libros = data;
      });
  }
  ngOnInit() {
    this.autentificado = JSON.parse(this.autentificacion.obtenerAutentificado());
    console.log(JSON.parse(this.autentificacion.obtenerAutentificado()));
    console.log('OBTENER AUTENTIFICADO');
    console.log(this.autentificado.dni);
    
  }

  modalBoton(id : string) {
    this.prestamoService.getItem(id).subscribe(data =>{
      console.log(data.items);
      this.itemData = data.items;
    });
  }
  
  guardar(){        
    this.prestamo.estado = '1';
    this.prestamo.usuarioId = this.autentificado.dni;
    console.log(this.prestamo);

    this.prestamoService.newPedido(this.prestamo).subscribe(data =>{
      console.log('Respuesta de servicios post');  
      console.log(data);
    });

   /* this.prestamoService.getItem().subscribe(data =>{
      console.log(data.items);
      this.itemData = data.items;
    });*/

  }

}
