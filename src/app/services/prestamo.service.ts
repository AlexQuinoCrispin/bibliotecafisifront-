import { Injectable } from '@angular/core';
import { Http , Headers} from '@angular/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Prestamo } from '../interfaces/prestamo.interface';

@Injectable({
  providedIn: 'root'
})
export class PrestamoService {
  librosURL: string = 'https://bibliotecabackend.herokuapp.com/libros?' +
  'Content-Type=application/json&clave=QDm6pbKeVwWikPvpMSUYwp0tNnxcaLoY' +
  'LnyvLQ4ISV39uQOgsjTEjS0UNlZHwbxl2Ujf30S31CSKndwpkFeubt5gJHTgFlq7LeIaS' +
  'Yc0jNm44loPty2ZK1nI0qisrt2Xwq0nFhdp8H3kdpyL5wVZLH7EpSE6IO0cHAOGOfSpJjF3' +
  '6eiCuXJ3gkOfX8C4n';

  clave: string = 'Content-Type=application/json&clave=QDm6pbKeVwWikPvpMSUYwp0tNnxcaLoY' +
  'LnyvLQ4ISV39uQOgsjTEjS0UNlZHwbxl2Ujf30S31CSKndwpkFeubt5gJHTgFlq7LeIaS' +
  'Yc0jNm44loPty2ZK1nI0qisrt2Xwq0nFhdp8H3kdpyL5wVZLH7EpSE6IO0cHAOGOfSpJjF3' +
  '6eiCuXJ3gkOfX8C4n';

  libroURL: string = 'https://bibliotecabackend.herokuapp.com/libros';

  prestamoURL : string = 'http://bibliotecabackend.herokuapp.com/pedidos';
  
  
 constructor(private http: Http, private router: Router) {
    console.log('Servicio Libro Listo');
  }

  getLibros() {
    return this.http.get(this.librosURL)
        .pipe(map( res => res.json()));
  }

  getItem(id : string){
    return this.http.get(this.libroURL+"/todo/"+id+"?"+this.clave).pipe(map( res => res.json()));
  }
  
  newPedido(prestamo : Prestamo){
    console.log(prestamo);
    const body = JSON.stringify( prestamo );
    const headers =  new  Headers({
      'Content-Type': 'application/json'
    });

    return this.http.post( this.prestamoURL+"?"+this.clave, body, { headers } )
        .pipe(map( res => {
          console.log(res.json());
          return res.json();
        }));
  }

}
