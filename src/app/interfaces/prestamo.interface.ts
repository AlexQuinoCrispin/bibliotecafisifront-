export interface Prestamo {
    usuarioId: string;
    itemId: string;
    fechaInicio: string;
    fechafin: string;
    estado: string;
    adminId: string;
}